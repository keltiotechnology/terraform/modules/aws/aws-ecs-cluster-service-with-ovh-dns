<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
# Aws ECS cluster service

## Farget info
Supported task CPU and memory values for tasks that are hosted on Fargate are as follows.

CPU value	Memory value (MiB)
256 (.25 vCPU)	512 (0.5GB), 1024 (1GB), 2048 (2GB)
512 (.5 vCPU)	1024 (1GB), 2048 (2GB), 3072 (3GB), 4096 (4GB)
1024 (1 vCPU)	2048 (2GB), 3072 (3GB), 4096 (4GB), 5120 (5GB), 6144 (6GB), 7168 (7GB), 8192 (8GB)
2048 (2 vCPU)	Between 4096 (4GB) and 16384 (16GB) in increments of 1024 (1GB)
4096 (4 vCPU)	Between 8192 (8GB) and 30720 (30GB) in increments of 1024 (1GB)
https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task-cpu-memory-error.html

## Usage

```hcl-terraform
data "aws_vpc" "vpc" {
  filter {
    name   = "tag:Name"
    values = ["keltio-all-vpc"]
  }
}

data "aws_subnet_ids" "private_subnets" {
  vpc_id = data.aws_vpc.vpc.id
  tags   = {
    "namespace" = "keltio"

    "stage"     = "dev"
    "tier"      = "Private"
  }
}
data "aws_security_group" "ecs_security_group" {
  name = "keltio-dev_ecs"
}

module "dev_keltio_frontend" {
  source                  = "../modules/ecs_cluster_service"

  namespace               = "keltio"
  stage                   = "dev"
  app_name                = "keltio-dev-frontend"
  app_fqdn                = "dev.keltio.fr"

  cloudfront_distribution = "XXXXXXXXXXX"

  # Container info
  #############################

  app_container_name      = "react_container"
  app_image_name          = var.app_image_name
  app_image_tag           = var.app_image_tag
  # When networkMode=awsvpc, the host ports and container ports in port mappings must match
  app_host_port           = 3000
  app_container_port      = 3000

  app_container_cpu       = 256
  app_container_memory    = 512
  app_reserved_cpu        = 256
  app_reserved_memory     = 512
  app_desired_count       = 1
  app_container_command   = []
  app_heathcheck_path     = "/"

  app_env_vars            = {
    API_URL   = "https://dev.api.keltio.fr"
    ENV       = "dev"
    ECS_AVAILABLE_LOGGING_DRIVERS = "[\"json-file\",\"awslogs\"]"
  }

  # Other settings
  #############################
  alb_priority              = 100
  app_target_group          = "dev-front-tg"
  cloudwatch_log_group_name = "dev-front-logs"
  security_groups           = [data.aws_security_group.ecs_security_group.id]
  subnets                   = data.aws_subnet_ids.private_subnets.ids
}
```

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.1 |
| <a name="requirement_null"></a> [null](#requirement\_null) | >= 2.0 |
| <a name="requirement_ovh"></a> [ovh](#requirement\_ovh) | >= 0.13.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.56.0 |
| <a name="provider_ovh"></a> [ovh](#provider\_ovh) | 0.15.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_application_container"></a> [application\_container](#module\_application\_container) | cloudposse/ecs-container-definition/aws | 0.57.0 |

## Resources

| Name | Type |
|------|------|
| [aws_alb_listener_rule.http](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/alb_listener_rule) | resource |
| [aws_alb_listener_rule.https](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/alb_listener_rule) | resource |
| [aws_cloudwatch_log_group.ecs_log_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | resource |
| [aws_ecs_service.ec2_service](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_service) | resource |
| [aws_ecs_service.fargate_service](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_service) | resource |
| [aws_ecs_task_definition.application_task](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_task_definition) | resource |
| [ovh_domain_zone_record.service_record](https://registry.terraform.io/providers/ovh/ovh/latest/docs/resources/domain_zone_record) | resource |
| [ovh_domain_zone_record.www_service_record](https://registry.terraform.io/providers/ovh/ovh/latest/docs/resources/domain_zone_record) | resource |
| [aws_cloudfront_distribution.entrypoint](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/cloudfront_distribution) | data source |
| [aws_ecs_cluster.ecs_cluster](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ecs_cluster) | data source |
| [aws_iam_role.ecs_service_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_role) | data source |
| [aws_iam_role.ecs_task_service_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_role) | data source |
| [aws_lb.ecs_load_balancer](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/lb) | data source |
| [aws_lb_listener.http](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/lb_listener) | data source |
| [aws_lb_listener.https](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/lb_listener) | data source |
| [aws_lb_target_group.application_target_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/lb_target_group) | data source |
| [aws_vpc.ecs_vpc](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/vpc) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_alb_priority"></a> [alb\_priority](#input\_alb\_priority) | Load balancer priority | `number` | `100` | no |
| <a name="input_app_container_command"></a> [app\_container\_command](#input\_app\_container\_command) | The port exposed by the app | `list(string)` | n/a | yes |
| <a name="input_app_container_cpu"></a> [app\_container\_cpu](#input\_app\_container\_cpu) | The port exposed by the app | `string` | n/a | yes |
| <a name="input_app_container_memory"></a> [app\_container\_memory](#input\_app\_container\_memory) | The port exposed by the app | `string` | n/a | yes |
| <a name="input_app_container_name"></a> [app\_container\_name](#input\_app\_container\_name) | The container name in AWS ECS | `string` | n/a | yes |
| <a name="input_app_container_port"></a> [app\_container\_port](#input\_app\_container\_port) | The port exposed by the app | `string` | n/a | yes |
| <a name="input_app_desired_count"></a> [app\_desired\_count](#input\_app\_desired\_count) | The port exposed by the app | `string` | n/a | yes |
| <a name="input_app_env_secrets"></a> [app\_env\_secrets](#input\_app\_env\_secrets) | Secrets exposed as environnement variables in container | `map(string)` | `{}` | no |
| <a name="input_app_env_vars"></a> [app\_env\_vars](#input\_app\_env\_vars) | Environnement variables exposed in container | `map(string)` | `{}` | no |
| <a name="input_app_fqdn"></a> [app\_fqdn](#input\_app\_fqdn) | App FQDN | `string` | n/a | yes |
| <a name="input_app_heathcheck_path"></a> [app\_heathcheck\_path](#input\_app\_heathcheck\_path) | Heathcheck path for application service | `string` | n/a | yes |
| <a name="input_app_host_port"></a> [app\_host\_port](#input\_app\_host\_port) | The port the expose the app | `string` | n/a | yes |
| <a name="input_app_image_name"></a> [app\_image\_name](#input\_app\_image\_name) | The docker image name of the application | `string` | n/a | yes |
| <a name="input_app_image_tag"></a> [app\_image\_tag](#input\_app\_image\_tag) | The docker image tag of the application | `string` | n/a | yes |
| <a name="input_app_launch_type"></a> [app\_launch\_type](#input\_app\_launch\_type) | App ecs requires compatibilites : Set of launch types required by the task. The valid values are FARGATE or EC2 | `string` | `"FARGATE"` | no |
| <a name="input_app_name"></a> [app\_name](#input\_app\_name) | Name of the application | `string` | n/a | yes |
| <a name="input_app_network_mode"></a> [app\_network\_mode](#input\_app\_network\_mode) | App network mode : The networking behavior of Amazon ECS tasks hosted on Amazon EC2 instances is dependent on the network mode defined in the task definition.. The valid values are awsvpc when app\_launch\_type is FARGATE and bridge when app\_launch\_type is EC2 | `string` | `"awsvpc"` | no |
| <a name="input_app_reserved_cpu"></a> [app\_reserved\_cpu](#input\_app\_reserved\_cpu) | The port exposed by the app | `string` | n/a | yes |
| <a name="input_app_reserved_memory"></a> [app\_reserved\_memory](#input\_app\_reserved\_memory) | The port exposed by the app | `string` | n/a | yes |
| <a name="input_app_target_group"></a> [app\_target\_group](#input\_app\_target\_group) | App load balancer target group | `string` | n/a | yes |
| <a name="input_application_load_balancer_name"></a> [application\_load\_balancer\_name](#input\_application\_load\_balancer\_name) | Load balancer name for the app | `string` | `"alb"` | no |
| <a name="input_cloudfront_distribution"></a> [cloudfront\_distribution](#input\_cloudfront\_distribution) | Cloudfront distribution used to render the service | `string` | n/a | yes |
| <a name="input_cloudwatch_log_group_name"></a> [cloudwatch\_log\_group\_name](#input\_cloudwatch\_log\_group\_name) | Name of the Cloudwatch log group for ecs | `string` | `"ecs-logs"` | no |
| <a name="input_cloudwatch_log_retention_in_days"></a> [cloudwatch\_log\_retention\_in\_days](#input\_cloudwatch\_log\_retention\_in\_days) | Cloudwatch log retention for the service (in days) | `number` | `30` | no |
| <a name="input_ecs_cluster_name"></a> [ecs\_cluster\_name](#input\_ecs\_cluster\_name) | ECS cluster name of the app | `string` | `"ecs"` | no |
| <a name="input_ecs_task_family"></a> [ecs\_task\_family](#input\_ecs\_task\_family) | Ecs task family name to create | `string` | `"FARGATE"` | no |
| <a name="input_iam_ecs_instance_role_name"></a> [iam\_ecs\_instance\_role\_name](#input\_iam\_ecs\_instance\_role\_name) | Environnement variables exposed in container | `string` | `"ecsInstanceRole"` | no |
| <a name="input_iam_ecs_task_execution_role_name"></a> [iam\_ecs\_task\_execution\_role\_name](#input\_iam\_ecs\_task\_execution\_role\_name) | Environnement variables exposed in container | `string` | `"ecsTaskExecutionRole"` | no |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | Namespace, which could be your organization name or abbreviation, e.g. 'eg' or 'cp' | `string` | `"swappy"` | no |
| <a name="input_region"></a> [region](#input\_region) | AWS region | `string` | `"eu-central-1"` | no |
| <a name="input_security_groups"></a> [security\_groups](#input\_security\_groups) | Security groups ids used for ecs service tasks | `list(string)` | n/a | yes |
| <a name="input_stage"></a> [stage](#input\_stage) | Stage, which could be 'test' 'dev' 'prod' ... | `string` | `"dev"` | no |
| <a name="input_subnets"></a> [subnets](#input\_subnets) | Subnets ids where the ecs service will run | `list(string)` | `[]` | no |
| <a name="input_vpc_name"></a> [vpc\_name](#input\_vpc\_name) | VPC used for the ECS cluster | `string` | `"vpc"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_app_fqdn"></a> [app\_fqdn](#output\_app\_fqdn) | n/a |
| <a name="output_dns_subdomains"></a> [dns\_subdomains](#output\_dns\_subdomains) | n/a |
| <a name="output_dns_zone"></a> [dns\_zone](#output\_dns\_zone) | n/a |
| <a name="output_ecs_vpc"></a> [ecs\_vpc](#output\_ecs\_vpc) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
