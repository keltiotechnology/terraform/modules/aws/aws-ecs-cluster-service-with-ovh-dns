/*
 * # Aws ECS cluster service
 * 
 * ## Farget info
 * Supported task CPU and memory values for tasks that are hosted on Fargate are as follows.
 * 
 * CPU value	Memory value (MiB)
 * 256 (.25 vCPU)	512 (0.5GB), 1024 (1GB), 2048 (2GB)
 * 512 (.5 vCPU)	1024 (1GB), 2048 (2GB), 3072 (3GB), 4096 (4GB)
 * 1024 (1 vCPU)	2048 (2GB), 3072 (3GB), 4096 (4GB), 5120 (5GB), 6144 (6GB), 7168 (7GB), 8192 (8GB)
 * 2048 (2 vCPU)	Between 4096 (4GB) and 16384 (16GB) in increments of 1024 (1GB)
 * 4096 (4 vCPU)	Between 8192 (8GB) and 30720 (30GB) in increments of 1024 (1GB)
 * https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task-cpu-memory-error.html
 *
 * ## Usage
 * 
 * ```hcl-terraform
 * data "aws_vpc" "vpc" {
 *   filter {
 *     name   = "tag:Name"
 *     values = ["keltio-all-vpc"]
 *   }
 * }
 *
 * data "aws_subnet_ids" "private_subnets" {
 *   vpc_id = data.aws_vpc.vpc.id
 *   tags   = {
 *     "namespace" = "keltio"
 * 
 *     "stage"     = "dev"
 *     "tier"      = "Private"
 *   }
 * }
 * data "aws_security_group" "ecs_security_group" {
 *   name = "keltio-dev_ecs"
 * }
 *
 * module "dev_keltio_frontend" {
 *   source                  = "../modules/ecs_cluster_service"
 * 
 *   namespace               = "keltio"
 *   stage                   = "dev"
 *   app_name                = "keltio-dev-frontend"
 *   app_fqdn                = "dev.keltio.fr"
 * 
 *   cloudfront_distribution = "XXXXXXXXXXX"
 *
 *   # Container info
 *   #############################
 * 
 *   app_container_name      = "react_container"
 *   app_image_name          = var.app_image_name
 *   app_image_tag           = var.app_image_tag
 *   # When networkMode=awsvpc, the host ports and container ports in port mappings must match
 *   app_host_port           = 3000
 *   app_container_port      = 3000
 * 
 *   app_container_cpu       = 256
 *   app_container_memory    = 512
 *   app_reserved_cpu        = 256
 *   app_reserved_memory     = 512
 *   app_desired_count       = 1
 *   app_container_command   = []
 *   app_heathcheck_path     = "/"
 * 
 *   app_env_vars            = {
 *     API_URL   = "https://dev.api.keltio.fr"
 *     ENV       = "dev"
 *     ECS_AVAILABLE_LOGGING_DRIVERS = "[\"json-file\",\"awslogs\"]"
 *   }
 * 
 *   # Other settings
 *   #############################
 *   alb_priority              = 100
 *   app_target_group          = "dev-front-tg"
 *   cloudwatch_log_group_name = "dev-front-logs"
 *   security_groups           = [data.aws_security_group.ecs_security_group.id]
 *   subnets                   = data.aws_subnet_ids.private_subnets.ids
 * }
 * ```
*/

####################################################################
#
# Load AWS data
#
####################################################################
data "aws_vpc" "ecs_vpc" {
  filter {
    name   = "tag:Name"
    values = [var.vpc_name]
  }
}

data "aws_ecs_cluster" "ecs_cluster" {
  cluster_name = var.ecs_cluster_name
}

data "aws_cloudfront_distribution" "entrypoint" {
  id = var.cloudfront_distribution
}

data "aws_lb" "ecs_load_balancer" {
  name = var.application_load_balancer_name
}

data "aws_lb_listener" "http" {
  load_balancer_arn = data.aws_lb.ecs_load_balancer.arn
  port              = "80"
}

data "aws_lb_listener" "https" {
  load_balancer_arn = data.aws_lb.ecs_load_balancer.arn
  port              = "443"
}

data "aws_iam_role" "ecs_service_role" {
  name = var.iam_ecs_instance_role_name
}

data "aws_iam_role" "ecs_task_service_role" {
  name = var.iam_ecs_task_execution_role_name
}

data "aws_lb_target_group" "application_target_group" {
  name = var.app_target_group
}


####################################################################
#
# Application logging settings
#
####################################################################
resource "aws_cloudwatch_log_group" "ecs_log_group" {
  name              = var.cloudwatch_log_group_name
  retention_in_days = var.cloudwatch_log_retention_in_days
}


####################################################################
#
# Application ECS settings
#
####################################################################
module "application_container" {
  source          = "cloudposse/ecs-container-definition/aws"
  version         = "0.57.0"
  container_image = "${var.app_image_name}:${var.app_image_tag}"
  container_name  = var.app_container_name

  container_cpu    = var.app_container_cpu
  container_memory = var.app_container_memory

  port_mappings = [
    {
      containerPort = var.app_container_port
      hostPort      = var.app_host_port
      protocol      = "tcp"
    }
  ]

  map_environment = var.app_env_vars
  map_secrets     = var.app_env_secrets

  command = var.app_container_command

  log_configuration = {
    "logDriver" : "awslogs",
    "options" : {
      "awslogs-region" : var.region,
      "awslogs-group" : var.cloudwatch_log_group_name,
      "awslogs-stream-prefix" : "ecs"
    }
  }
}

resource "aws_ecs_task_definition" "application_task" {
  family = "${var.app_name}-template"

  cpu    = var.app_reserved_cpu
  memory = var.app_reserved_memory
  container_definitions = jsonencode([
    module.application_container.json_map_object
  ])
  requires_compatibilities = [var.app_launch_type]
  network_mode             = var.app_network_mode
  execution_role_arn       = data.aws_iam_role.ecs_task_service_role.arn
}

resource "aws_ecs_service" "fargate_service" {
  count = var.app_launch_type == "FARGATE" ? 1 : 0

  cluster     = data.aws_ecs_cluster.ecs_cluster.id
  launch_type = "FARGATE"

  name            = var.app_name
  task_definition = aws_ecs_task_definition.application_task.arn
  desired_count   = var.app_desired_count

  network_configuration {
    subnets          = var.subnets
    security_groups  = var.security_groups
    assign_public_ip = false
  }

  load_balancer {
    target_group_arn = data.aws_lb_target_group.application_target_group.arn
    container_name   = var.app_container_name
    container_port   = var.app_container_port
  }
}

resource "aws_ecs_service" "ec2_service" {
  count = var.app_launch_type == "EC2" ? 1 : 0

  cluster     = data.aws_ecs_cluster.ecs_cluster.id
  launch_type = "EC2"

  name            = var.app_name
  task_definition = aws_ecs_task_definition.application_task.arn

  // Do not set when app_launch_type is FARGATE
  iam_role      = data.aws_iam_role.ecs_service_role.arn
  desired_count = var.app_desired_count

  load_balancer {
    target_group_arn = data.aws_lb_target_group.application_target_group.arn
    container_name   = var.app_container_name
    container_port   = var.app_container_port
  }
}

####################################################################
#
# Application load balancer settings
#
####################################################################
resource "aws_alb_listener_rule" "http" {
  listener_arn = data.aws_lb_listener.http.arn
  priority     = var.alb_priority

  action {
    type             = "forward"
    target_group_arn = data.aws_lb_target_group.application_target_group.arn
  }

  condition {
    host_header {
      values = [
        var.app_fqdn,
        "www.${var.app_fqdn}"
      ]
    }
  }
}

resource "aws_alb_listener_rule" "https" {
  listener_arn = data.aws_lb_listener.https.arn
  priority     = var.alb_priority

  action {
    type             = "forward"
    target_group_arn = data.aws_lb_target_group.application_target_group.arn
  }

  condition {
    host_header {
      values = [
        var.app_fqdn,
        "www.${var.app_fqdn}"
      ]
    }
  }
}
